#include <iostream>

class Szulo {

public:
	void kiir() {
		std::cout << "szulo" << std::endl;
	}

};

class Gyerek: public Szulo {

public:
	void kiir() {
		std::cout << "gyerek" << std::endl;
	}
	void kiir2() {
		std::cout << "gyerek2" << std::endl;
	}

};

void f(Szulo& sz) {
	sz.kiir();
}

class Szulo2 {

public:
	virtual void kiir() {
		std::cout << "szulo" << std::endl;
	}

};

class Gyerek2 : public Szulo2 {

public:
	void kiir() {
		std::cout << "gyerek" << std::endl;
	}

};

void f2(Szulo2& sz) {
	sz.kiir();
}

void f3(Szulo2 sz) {
	sz.kiir();
}

int main() {
	Szulo szulo;
	Gyerek gyerek;
	Szulo* szgyerek = new Gyerek();

	f(szulo);
	f(gyerek);
	f(*szgyerek);

	//gyerek.kiir();
	//szulo.kiir2();
	//gyerek.kiir2();
	//szgyerek->kiir2();
	

	std::cout << "---------------" << std::endl;

	Szulo2 szulo2;
	Gyerek2 gyerek2;
	Szulo2* szgyerek2 = new Gyerek2();

	f2(szulo2);
	f2(gyerek2);
	f2(*szgyerek2);

	std::cout << "---------------" << std::endl;
	f3(szulo2);
	f3(gyerek2);
	f3(*szgyerek2);

}

