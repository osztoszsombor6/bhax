package liskov;

public class Liskov {
    
    static void tankolas(Auto a) {
        System.out.println("Tankolás");
        a.tankolas();
    }
    
    static void benzintankolas(BenzinUzemuAuto a) {
        a.tankolas();
    }

    public static void main(String[] args) {
        Auto auto = new Auto();
        HibridAuto hibrid = new HibridAuto();
        EAuto eauto = new EAuto();
        
        tankolas(auto);
        tankolas(hibrid);
        tankolas(eauto);
        
        JobbBenzines jba = new JobbBenzines();
        jba.tankolas();
        JobbHibridAuto jha = new JobbHibridAuto();
        jha.akkuToltes();
        jha.tankolas();
        JobbEAuto jea = new JobbEAuto();
        jea.akkuToltes();
        
        benzintankolas(jba);
        benzintankolas(jha);
    }
    
}
