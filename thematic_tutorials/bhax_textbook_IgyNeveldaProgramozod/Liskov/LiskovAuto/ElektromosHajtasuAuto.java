package liskov;

public interface ElektromosHajtasuAuto {
    
    default public void akkuToltes() {
        System.out.println("Akku töltése");
    }
    
}
