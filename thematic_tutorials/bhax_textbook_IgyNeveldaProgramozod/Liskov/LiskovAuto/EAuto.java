package liskov;

public class EAuto extends HibridAuto {
    
    @Override
    public void tankolas() {
        throw new RuntimeException("Nincs tankolásra lehetőség!");
    }
}
