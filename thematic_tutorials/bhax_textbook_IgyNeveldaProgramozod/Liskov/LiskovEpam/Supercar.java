
package liskov2;

public class Supercar extends Car {
    public Supercar() {
        System.out.println("Supercar konstruktor");
    }
        
    @Override
    void start(){
        System.out.println("Supercar start");
    }
}
