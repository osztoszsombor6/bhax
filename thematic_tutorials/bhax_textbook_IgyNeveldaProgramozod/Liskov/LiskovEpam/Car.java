
package liskov2;


public class Car extends Vehicle {

    public Car() {
        System.out.println("Car konstruktor");
    }
    
    @Override
    void start(){
        System.out.println("Car start");
    }
}
