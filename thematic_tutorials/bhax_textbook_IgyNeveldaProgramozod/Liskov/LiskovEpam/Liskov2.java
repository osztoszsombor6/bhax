
package liskov2;

public class Liskov2 {


    public static void main(String[] args) {
        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);
        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();
        System.out.println(secondVehicle instanceof Supercar);
        //Supercar thirdVehicle = new Vehicle();
        //thirdVehicle.start();
    }
    
}
