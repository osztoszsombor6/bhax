public class LZWBinFa {
    
    private Csomopont fa;
    int melyseg, atlagosszeg, atlagdb;
    double szorasosszeg;
    
    protected Csomopont gyoker;
    int maxMelyseg;
    double atlag, szoras;
    
    public LZWBinFa() {
        // : fa(&gyoker) {};
        gyoker = new Csomopont('/');
        fa = gyoker;
    }
    
    public void beszur(String s) {
        if (s != null && !s.isEmpty()) {
            for (char c: s.toCharArray()) {
                beszur(c);
            }
        }
    }
    
    public void beszur(char b) {
        if (b == '0')
        {
            if (fa.nullasGyermek() == null) 
            {
                Csomopont uj = new Csomopont('0');
                fa.ujNullasGyermek(uj);
                fa = gyoker;
            }
            else 
            {
                fa = fa.nullasGyermek();
            }
        }
        else
        {
            if (fa.egyesGyermek() == null)
            {
                Csomopont uj = new Csomopont('1');
                fa.ujEgyesGyermek(uj);
                fa = gyoker;
            }
            else
            {
                fa = fa.egyesGyermek();
            }
        }
    }
    
    public void kiir() {
        melyseg = 0;
        kiir(gyoker);
        System.out.println("depth = " + getMelyseg());
        System.out.println("mean = " + getAtlag());
        System.out.println("var = " + getSzoras());
    }
    
    private void kiir(Csomopont elem) {
        if (elem != null) {
            ++melyseg;
            kiir(elem.egyesGyermek());
            for (int i = 0; i < melyseg; ++i) {
                System.out.print("---");
            }
            System.out.println(elem.getBetu() + "(" + (melyseg - 1) + ")");
            kiir(elem.nullasGyermek());
            --melyseg;
        }
    }
    
    public int getMelyseg() {
        melyseg = maxMelyseg = 0;
        rmelyseg(gyoker);
        return maxMelyseg - 1;
    }
    
    protected void rmelyseg(Csomopont elem) {
        if (elem != null) {
            ++melyseg;
            if (melyseg > maxMelyseg) {
                maxMelyseg = melyseg;
            }
            rmelyseg(elem.egyesGyermek());
            rmelyseg(elem.nullasGyermek());
            melyseg--;
        }
    }
    
    public double getSzoras() {
        atlag = getAtlag();
        szorasosszeg = 0.0;
        melyseg = atlagdb = 0;
        rszoras(gyoker);
        if (atlagdb - 1 > 0) {
            szoras = Math.sqrt(szorasosszeg / (atlagdb - 1));
        } else {
            szoras = Math.sqrt(szorasosszeg);
        }
        return szoras;
    }
    
    protected void rszoras(Csomopont elem) {
        if (elem != null) {
            ++melyseg;
            rszoras(elem.egyesGyermek());
            rszoras(elem.nullasGyermek());
            melyseg--;
            if (elem.egyesGyermek() == null && elem.nullasGyermek() == null) {
                ++atlagdb;
                szorasosszeg += ((melyseg - atlag) * (melyseg - atlag));
            }
        }
    }
    
    public double getAtlag() {
        melyseg = atlagosszeg = atlagdb = 0;
        ratlag(gyoker);
        atlag = ((double)atlagosszeg) / atlagdb;
        return atlag;
    }
    
    private void ratlag(Csomopont elem) {
        if (elem != null) {
            ++melyseg;
            ratlag(elem.egyesGyermek());
            ratlag(elem.nullasGyermek());
            --melyseg;
            if (elem.egyesGyermek() == null && elem.nullasGyermek() == null) {
                ++atlagdb;
                atlagosszeg += melyseg;
            }
        }
    }
    
    static protected class Csomopont {
        
        private final char betu;
        private Csomopont balNulla;
        private Csomopont jobbEgy;
        
        public Csomopont(char b) {
            betu = b;
        }
        
        public Csomopont nullasGyermek() {
            return balNulla;
        }
        
        public Csomopont egyesGyermek() {
            return jobbEgy;
        }
        
        public void ujNullasGyermek(Csomopont gy) {
            balNulla = gy;
        }
        
        public void ujEgyesGyermek(Csomopont gy) {
            jobbEgy = gy;
        }
        
        public char getBetu() {
            return betu;
        }
        
    }
    

    public static void main(String[] args) {
        LZWBinFa fa = new LZWBinFa();
        fa.beszur("01111001001001000111");
        fa.kiir();
    }
    
}