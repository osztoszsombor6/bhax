import lzwbinfa.LZWBinFa;
import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;


public class LZWBinFaTest {
    
    static LZWBinFa fa;
    
    @Before
    public void before(){
    System.out.println("Before");
    }
    
    @BeforeClass
    public static void alapBinFaLetrehozas(){
        System.out.println("BeforeClass");
        fa = new LZWBinFa();
        fa.beszur("01111001001001000111");
    }
    
    @Test
    public void szorasTest(){
          
        double szoras = fa.getSzoras();
        Assert.assertEquals(0.9574, szoras, 0.0001);
    }
    
    @Test
    public void atlagTest(){
       
        double atlag = fa.getAtlag();
        Assert.assertEquals(2.75, atlag, 0.000001);
    }
    
    @Test
    public void melysegTest(){
        
        int melyseg = fa.getMelyseg();
        Assert.assertEquals(4, melyseg);
    }
    
    @Test
    public void uresMelysegTest(){
        LZWBinFa fa = new LZWBinFa();
        int melyseg = fa.getMelyseg();
        Assert.assertEquals(0, melyseg);
    }
    
}