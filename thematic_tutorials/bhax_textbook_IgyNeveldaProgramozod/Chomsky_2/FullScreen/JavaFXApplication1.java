import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class JavaFXApplication1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("'Hello World' kiirása");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        /* Egy gomb létrehozása, amelynek megnyomásával
        Hello World-szöveget írhatunk a kimenetre*/
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        
        Scene scene = new Scene(root, 300, 250);
        /* A kirajzolt ablak szélességét és magasságát
        adhatjuk meg a root után, bár ez csak a 
        teljes képernyős módból kilépés esetén látható*/
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(true);
        /* A setFullScreen(true); megadásával a létrehozott
        ablak teljes képernyőn fut*/
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
