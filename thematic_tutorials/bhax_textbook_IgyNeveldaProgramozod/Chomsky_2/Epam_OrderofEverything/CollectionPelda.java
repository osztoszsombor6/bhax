import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionPelda {
    
    static class A {
        A(int aa)  { a = aa; }
        public int a;
    };
    
    public static List<Integer> createorderedList2(Collection<Integer> l) {
        List<Integer> eredmeny = new ArrayList<>(l.size());
        eredmeny.addAll(l);
        //eredmeny = new ArrayList<>(l);
        Collections.sort(eredmeny);
        return eredmeny;
    }
    
    public static <T extends Comparable> List<T> createorderedList(Collection<T> l) {
        List<T> eredmeny = new ArrayList<>(l.size());
        eredmeny.addAll(l);
        //eredmeny = new ArrayList<>(l);
        Collections.sort(eredmeny);
        return eredmeny;
    }

    public static void main(String[] args) {
        List<Integer> l = new ArrayList<>();
        
        l.add(1);
        l.add(10);
        l.add(5);
        
        //List nyl = l;
        //nyl.add("tralala");
        
        Set<Integer> ls = new HashSet<>();
        ls.add(1);
        ls.add(10);
        ls.add(5);
        
        for (Integer i: l) {
            System.out.println(i);
        }
        
        List<Integer> l2 = createorderedList(l);
        
        System.out.println("-----------Rendezett createOrderedList------------");
        for (Integer i: l2) {
            System.out.println(i);
        }
        
        Collections.sort(l);
        System.out.println("-----------Rendezett------------");
        for (Integer i: l) {
            System.out.println(i);
        }
        
        
        List ll = new ArrayList();
        ll.add(18);
        ll.add("tralala");
        
        //Collections.sort(ll);
        //List<Integer> l3 = createorderedList(ll);
        
        List<A> la = new ArrayList<>();
        la.add(new A(1));
        la.add(new A(10));
        la.add(new A(5));
        
        //List<Integer> l4 = createorderedList(la);
        
        
        List<Object> l5 = new ArrayList<>();
        l5.add(18);
        l5.add("tralala");
        
        //Collections.sort(ll);
        //List<Integer> l5r = createorderedList(l5);
    }
    
}
