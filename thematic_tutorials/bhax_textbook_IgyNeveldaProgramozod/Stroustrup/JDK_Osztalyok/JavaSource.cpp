#include "boost/filesystem.hpp"
#include <iostream>

int main () {
  int db=0;
  for ( boost::filesystem::recursive_directory_iterator end, dir("./");
    dir != end; ++dir ) {
    std::string ext(".java");
    if (!ext.compare(boost::filesystem::extension(dir->path()))) {
		std::string s = dir->path().relative_path().string();
		std::string src="./src/";
		unsigned int pos = s.find(src);
		if(pos!=std::string::npos){
			s.replace(pos,src.length(),"");
			s = s.substr(0,s.length()-ext.length());
			for(int i=0; i<s.length();++i){
				if(s[i]=='/'){
					s[i]='.';
				}
			}
			++db;
			std::cout<<s<<std::endl;
		}
    }
  }
  std::cout<<"A talált osztályok száma: "<<db<<std::endl;
}
