import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.ArrayList;
import java.util.List;

public class OutofMemory {
    
    private static final int MEGABYTE = (1024*1024);
    public static void runOutOfMemory() {
        List<byte[]> l = new ArrayList<>();
        MemoryMXBean memoryBean = ManagementFactory.getMemoryMXBean();
        for (int i=1; i <= 100; i++) {
            try {
                byte[] bytes = new byte[MEGABYTE*500];
                l.add(bytes);
                System.out.println(i);
                MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
                long maxMemory = heapUsage.getMax() / MEGABYTE;
                long usedMemory = heapUsage.getUsed() / MEGABYTE;
                System.out.println(i+ " : Memory Use :" + usedMemory + "M/" +maxMemory+"M");
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                System.out.println("OutOfMemoryError");
                MemoryUsage heapUsage = memoryBean.getHeapMemoryUsage();
                long maxMemory = heapUsage.getMax() / MEGABYTE;
                long usedMemory = heapUsage.getUsed() / MEGABYTE;
                System.out.println(i+ " : Memory Use :" + usedMemory + "M/" +maxMemory+"M");
            }
        }
        System.out.println(l.size());
    }

    public static void main(String[] args) {
        runOutOfMemory();
    }
}
