import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class BugousStuffProducer implements AutoCloseable {
    private final Writer writer;
    
        @Override
    public void close() throws Exception {
        System.out.println("close...");
        writer.close();
    }
    
    public BugousStuffProducer(String outputFileName) throws  IOException {
        writer = new FileWriter(outputFileName);
    }

    public void writeStuff() throws IOException {
        writer.write("Stuff");
		//a writer.flush() meghívásával biztosíthatjuk, hogy 
		//a szöveg kiírása megtörténik.
        //writer.flush();
    }

    @Override
    public void finalize() throws IOException {
        System.out.println("finalize...");
        writer.close();
    }
    
    
    public static void main(String[] args) throws IOException, Exception {
//        BugousStuffProducer b = new BugousStuffProducer("teszt.txt");
//        b.writeStuff();

        //az AutoCloseable interfész implementálása után használhatunk
		//egy Java try with resources utasítást a megfelelő működés biztosításához
        try (BugousStuffProducer b = new BugousStuffProducer("teszt.txt")) {
            b.writeStuff();
        }
        //egy egyszerű try blokk finally ágát is használhatjuk
		// a close metódus meghívására
//        BugousStuffProducer b = new BugousStuffProducer("teszt.txt");
//        try {
//            b.writeStuff();
//        } finally {
//            System.out.println("finally close...");
//            b.writer.close();
//        }
    }
    
}