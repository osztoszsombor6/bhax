public class KindofEqual {

    public static void main(String[] args) {
        String first = "...";
        String second = "...";
        //String third = "...";
        String third = new String("...");
        //String third = new String(new char[]{'.', '.', '.'});
        // When
        Boolean firstMatchesSecondWithEquals = first.equals(second);
        Boolean firstMatchesSecondWithEqualToOperator = first == second;
        Boolean firstMatchesThirdWithEquals = first.equals(third);
        Boolean firstMatchesThirdWithEqualToOperator = first == third;
        
        System.out.println(firstMatchesSecondWithEquals);
        System.out.println(firstMatchesSecondWithEqualToOperator);
        System.out.println(firstMatchesThirdWithEquals);
        System.out.println(firstMatchesThirdWithEqualToOperator);
    }
    
}
