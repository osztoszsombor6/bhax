#include <iostream>
#include <string>
#include <vector>

template<typename T>
class CustomAlloc
{
public:
	using size_type = size_t;
	using pointer = T*;
	using const_pointer = const T*;
	using value_type = T;

	pointer allocate(size_type n)
	{
		std::cout << "Allocating "
			<< n << " objects of "
			<< sizeof(value_type)
			<< std::endl;

		return reinterpret_cast<pointer>(new char[n*sizeof(value_type)]);
	}

	void deallocate(pointer p, size_type n)
	{
		delete[] reinterpret_cast<char*> (p);
		std::cout << "Deallocating "
			<< n << " objects of "
			<< n * sizeof(value_type)
			<< std::endl;
	}
};



int main()
{

	std::vector<int, CustomAlloc<int>> v;

	std::cout << "42 elott" << std::endl;
	v.push_back(42);
	std::cout << "42 utan" << std::endl;
for (int i = 1; i < 5; ++i) {
		std::cout << i <<" elott" << std::endl;
		v.push_back(i);
	}

	return 0;
}
