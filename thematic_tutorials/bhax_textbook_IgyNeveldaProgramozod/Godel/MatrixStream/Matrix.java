import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class Matrix {
    protected final int[][] matrix;
	protected final int rowsLenght;
	protected final int columnsLenght;

	public Matrix(int[][] matrix) {
		this.matrix = matrix;
		this.rowsLenght = matrix.length;
		this.columnsLenght = matrix[0].length;
	}

	public Matrix(int rowsLenght, int columnsLenght) {
		this.matrix = new int[rowsLenght][columnsLenght];
		this.rowsLenght = rowsLenght;
		this.columnsLenght = columnsLenght;
	}


	public void setElement(int x, int y, int value) {
		matrix[x][y] = value;
	}

        protected Matrix multiply(Matrix input){
            
            /*int[] rr = this.matrix[0];
            int ii = 1;
            
            int ss = IntStream.range(0, input.rowsLenght).map(
                    jj -> rr[jj] * input.matrix[jj][ii]
            ).sum();*/
            
           
    

        int[][] result = Arrays.stream(this.matrix)
	   .map((r) -> { 
               return IntStream.range(0, input.columnsLenght)
                        .map( (i) -> {
                            return IntStream.range(0, input.rowsLenght).map( 
                                    (j) -> {
                                        return r[j] * input.matrix[j][i];
                                    }).sum();
                        })
                        .toArray(); 
           })
            .toArray(int[][]::new);
                    
		return new Matrix(result);
        }


    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + columnsLenght;
		result = prime * result + Arrays.deepHashCode(matrix);
		result = prime * result + rowsLenght;
		return result;
	}


    @Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Matrix)) {
			return false;
		}
		Matrix other = (Matrix) obj;
		if (columnsLenght != other.columnsLenght) {
			return false;
		}
		if (!Arrays.deepEquals(matrix, other.matrix)) {
			return false;
		}
		if (rowsLenght != other.rowsLenght) {
			return false;
		}
		return true;
	}


	public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                sb.append(rowsLenght);
                sb.append(", ");
                sb.append(columnsLenght);
                sb.append("]");
                for(int i = 0; i < rowsLenght; ++i){
                    sb.append("\n");
                    for(int j = 0; j < columnsLenght; j++){
                        
                        sb.append(matrix[i][j]);
                        sb.append(", ");
                    }
                }
                sb.append("\n");
		return sb.toString();
	}

    public static void main(String[] args) {
        int[][] m1={
            {1, 2, 3},
            {4, 5, 6}
        };
        
        int[][] m2={
            {7, 8},
            {9, 10},
            {11,12}
        };
        Matrix mat1 = new Matrix(m1);
        Matrix mat2 = new Matrix(m2);
        Matrix eredmeny = mat1.multiply(mat2);
        
        System.out.println(eredmeny);
        System.out.println(mat1);
        System.out.println(mat2);
    }
    
}