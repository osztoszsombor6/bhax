#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <sstream>

std::vector<std::pair<std::string, int>> sort_map(std::map <std::string, int>& rank)
{
	std::vector<std::pair<std::string, int>> ordered;

	for (auto& i : rank) {
		if (i.second) {
			std::pair<std::string, int> p{ i.first, i.second };
			ordered.push_back(p);
		}
	}

	std::sort(
		std::begin(ordered), std::end(ordered),
		[=](auto&& p1, auto&& p2) {
			//return p1.second > p2.second;
			std::cout << (p1.second < p2.second) << " " << p1.second << " " << p2.second << std::endl;
			return p1.second < p2.second;
		}
	);

	return ordered;
}

int main() {
	
	std::map <std::string, int> m;
	for (int i = 1; i < 10; ++i) {
		std::stringstream ss;
		//ss << 10 - i;
		ss << 2 * i;
		m[ss.str()] = i;
	}
	m["21"] = 5;
	for (auto& i : m) {
		std::cout << "first: " << i.first << " " << "second: " << i.second << std::endl;
	}
	std::cout << "---------------------" << std::endl;

	std::vector<std::pair<std::string, int>> v = sort_map(m);
	for (auto& i : v) {
		std::cout << "first: " << i.first << " " << "second: " << i.second << std::endl;
	}

	return 0;
}