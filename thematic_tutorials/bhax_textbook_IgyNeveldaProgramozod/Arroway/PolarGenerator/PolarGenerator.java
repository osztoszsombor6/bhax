package polartranszformator;

public class PolarGenerator {
    boolean vanTarolt;
    double tarolt;
    
    public PolarGenerator() {   
        vanTarolt = false;   
    }
    
    
    public double kovetkezo() {
        
        if(vanTarolt) {
            vanTarolt = false;
            return tarolt;
        } else {
        
            double u1, u2, v1, v2, w;
            do {
                u1 = Math.random();
                u2 = Math.random();
                
                v1 = 2*u1 - 1;
                v2 = 2*u2 - 1;
                
                w = v1*v1 + v2*v2;
                
            } while(w > 1);
            
            double r = Math.sqrt((-2*Math.log(w))/w);
            
            tarolt = r*v2;
            vanTarolt = true;
            
            return r*v1;
            
        }
    }
    
    public static void main(String[] args) {
        
        PolarGenerator g = new PolarGenerator();
        
        for(int i=0; i<100; ++i){
            System.out.println(g.kovetkezo());
        }
    }
}
