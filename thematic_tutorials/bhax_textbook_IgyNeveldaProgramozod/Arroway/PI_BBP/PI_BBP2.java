package bbp;

public class PI_BBP2 {
    
	// a formula modulo-t tartalmazó részének számolásához használt metódus
    static double binExpModK(double n, int k) {
        double t = 1;
        double r = 1;
        while (t <= n) {
            t *= 2;
        }
        //t /= 2;
        for (;;) {
            if (n >= t) {
                r = (16 * r) % k;
                n = n - t;
            }
            t = t / 2;
            if (t >= 1) {
                r = (r * r) % k;
            } else {
                break;
            }
        }
        return r;
    }
	// A PI hexa számjegyeinek kiszámítához négy részletben történik (S1,S4,S5,S6)
	// Ezek lesznek a formula szerint, megfelelő együtthatóval összegezve.
    static double Sj16d(int d, int j) {
        double sj = 0.0;
        
        for (int k = 0; k <= d; k++) {
            sj += (double) binExpModK(d - k, 8 * k + j) / (double)(8 * k + j);
        }
        //System.out.println(sj);
        
        for (int k = d + 1; k <= d * 2; k++) {
            sj += Math.pow(16, d - k) / (8* k + j);
        }
        
        sj = sj - Math.floor(sj);
        return sj;
    }
    
	// A hexadecimális számjegyek kiírását végzi a következő metódus
	
    static void print(double pi_nth, int jegyek) {
        for (int j = 0; j < jegyek; ++j) {
            int jegy = (int)Math.floor(16.0 *pi_nth);
            pi_nth = 16.0 * pi_nth - Math.floor(16.0 * pi_nth);
            System.out.print(Integer.toHexString(jegy));
        }
        System.out.println();
    }
    
    static void pi_bbp(int d, int jegyek) {
       
        double s1 = Sj16d(d, 1);  
        //System.out.println(s1 );
        double s4 = Sj16d(d, 4);
        double s5 = Sj16d(d, 5);
        double s6 = Sj16d(d, 6);
        
        double pi_nth = 4.0 * s1 - 2.0 * s4 - s5 -s6;
        pi_nth = pi_nth - Math.floor(pi_nth);
        //System.out.println(pi_nth);
        print(pi_nth, jegyek);
        
    }
    
    public static void main(String[] args) {
        pi_bbp(1000000, 10);
    }
    
}
